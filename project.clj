(defproject geneticrecom "0.1.0-SNAPSHOT"
  :description "some genetic learning experiment"
  :url "https://github.com/kasterma/geneticrecom"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.5.0"]
                 [org.clojure/data.json "0.2.1"]
                 [org.clojure/math.combinatorics "0.0.3"]
                 [org.clojure/tools.cli "0.2.2"]
                 [midje "1.5.0"]
                 [org.clojure/tools.trace "0.7.5"]
                 [com.taoensso/timbre "1.5.2"]
                 [net.mikera/core.matrix "0.6.0"]
                 [net.mikera/vectorz-clj "0.8.0"]]
  :jvm-opts ["-XX:MaxPermSize=128M"
             "-XX:+UseConcMarkSweepGC"
             "-Xms2g" "-Xmx2g" "-server"]
  ;; needed to use midje on travis
  :plugins [[lein-midje "3.0.0"]])
