(ns geneticrecom.core
  (:require [clojure.set :as set]
            [clojure.data.json :as json]
            [clojure.math.combinatorics :as combinat]
            [clojure.tools.cli :as cli]
            [clojure.tools.trace :as tt]
            [taoensso.timbre :as timbre
             :only (trace debug info warn error fatal spy)]
            [taoensso.timbre.profiling :as profiling :only (p profile)]
            [clojure.core.reducers :as r]
            [clojure.core.matrix :as matrix]
            [clojure.core.matrix.operators :as matrixo])
  (:use midje.sweet)
  (:gen-class))

(matrix/set-current-implementation :vectorz)

;; # Genetic recommendation
;;
;; This is an implementation of a bad idea in order to learn
;; identify bad behavior easier.
;;
;; Idea is to start with random feature vectors for people and movies,
;; and over time randomly perturb the features in a genetic algorithm
;; way.  I would expect very slow improvement.
;;
;; To learn:
;; 1. setting up an environment with random choices
;; 2. first implementation of something genetic

;; ## Setting the parameters for the environment
;;
;; Since if there is no pattern there is nothing to learn, we need to
;; generate data that has a learnable pattern (and then pretend we
;; don't know it while learning).
;;
;; In order to do this we'll randomly assign movies to two types, and
;; people to two types (liking one of the types of movies).  This is
;; a very binary situation, unlike real movie preferences, giving this
;; method an actual chance to succeed.

(def numpeople 400)
(def nummovies 30)

(def movietypes
  "random assignment of types to movies"
  (vec (repeatedly nummovies (fn [] (if (< (rand) 0.5) :A :B)))))

(def peopletypes
  "random assignment of types to people"
  (vec (repeatedly numpeople (fn [] (if (< (rand) 0.5) :A :B)))))

(def likes
  "initialize likes matrix to all zeros"
  (matrix/new-matrix numpeople nummovies))

(defn set-likes
  "set the likes matrix.

   1.0 if the movie and person type agree, 0.0 otherwise"
  [likes peopletypes movietypes]

  (doseq [p-idx (range 0 (count peopletypes))
          m-idx (range 0 (count movietypes))]
    (matrix/mset!
     likes p-idx m-idx
     (if (= (movietypes m-idx) (peopletypes p-idx))
       1
       0))))

(facts "set-likes sets the likes correctly"
  (let [mt   [:A :A :B]
        pt   [:A :B :A]
        lm   (matrix/new-matrix 3 3)
        _    (set-likes lm pt mt)]
    (tabular
     (fact (matrix/mget lm ?pi ?mi) => ?li)
     ?pi ?mi ?li
     0   0   1.0
     0   1   1.0
     0   2   0.0)))

;; ## The learning environment
;;
;; Even though we know there is one feature that determines all we
;; give every person and movie three features.

(defmacro dosetmat [mat form]
  `(doseq [i# (range 0 (matrix/row-count ~mat))
           j# (range 0 (matrix/column-count ~mat))]
     (matrix/mset! ~mat i# j# (~form i# j#))))

(def moviefeatures
  "feature vectors for movies"
  (matrix/new-matrix nummovies 3))

(def peoplefeatures
  "feature vectors for people"
  (matrix/new-matrix numpeople 3))

;; set all features randomly
(dosetmat moviefeatures (fn [_ _] (rand)))
(dosetmat peoplefeatures (fn [_ _] (rand)))

(defn row-prod
  [mat1 idx1 mat2 idx2]
  (let [row1    (matrix/slice mat1 idx1)
        row2    (matrix/slice mat2 idx2)]
    (matrix/dot row1 row2)))

(facts "row-prod computes the dot product of the indicated rows"
  (row-prod (matrix/matrix [[1 1 1] [2 2 2]]) 0
            (matrix/matrix [[1 1 1] [2 2 2]]) 0) => 3.0

  (row-prod (matrix/matrix [[1 1 1] [2 2 2]]) 1
            (matrix/matrix [[1 1 1] [2 2 2]]) 1) => 12.0
  (row-prod (matrix/matrix [[1 1 1] [2 2 2]]) 0
            (matrix/matrix [[2.0 1 0.3] [2 2 2]]) 0) => 3.3)

(defn compute-likes
  [pf mf]
  (let [likes   (matrix/new-matrix (matrix/row-count pf)
                                   (matrix/row-count mf))]
    (dosetmat likes (fn [i j] (row-prod pf i mf j)))
    likes))

(facts "likes are correctly computed"
  (let [l (compute-likes (matrix/matrix [[1 1] [0.1 0.1]])
                         (matrix/matrix [[1 2] [0.2 0.2]]))]
    (matrix/mget l 0 0) => 3.0
    (matrix/mget l 1 1) => (roughly 0.04 0.0001)
    (matrix/mget l 0 1) => 0.4))

(defn get-mutated-row
  "return a copy of a matrix row with one element changed"
  [delta mat row-idx]
  (let [row  (matrix/clone (matrix/slice mat row-idx))
        i    (rand-int (matrix/row-count row))
        v    (matrix/mget row i)
        nrow (matrix/mset row i ((if (< (rand) 0.5) - +) v delta))]
    nrow))

(defn apply-mutated-row
  [mat row-idx new-row]
  (doseq [j   (range 0 (matrix/row-count new-row))]
    (matrix/mset! mat row-idx j (matrix/mget new-row j))))

(facts "mutated row sets row"
  (let [m   (matrix/matrix [[1 1] [2 2]])
        nr  (matrix/matrix [3 4])
        m2  (matrix/clone m)
        _   (apply-mutated-row m2 0 nr)]
    (tabular
     (fact (matrix/mget m2 ?i ?j) => ?v)
     ?i  ?j  ?v
     0   0   3.0
     0   1   4.0
     1   0   2.0
     1   1   2.0)))

;; ## The learning algorithm
;;
;; Here we drive the learning to happen.

(def delta
  "change size while mutating"
  0.01)

(defn matrix-l2
  [mat]
  (let [m   (matrix/pow mat 2)
        ss  (matrix/esum m)
        l2  (Math/sqrt ss)]
    l2))

(facts "l2 norm computed correctly"
  (matrix-l2 (matrix/matrix [[1 1] [2 2]])) => (Math/sqrt 10.0)
  (matrix-l2 (matrix/matrix [[1 1] [-2 2]])) => (Math/sqrt 10.0))

(defn fit
  "Compute the current fit, i.e. the difference between actual likes
   and the likes computed based on the current features."

  []

  (let [likes-comp   (compute-likes peoplefeatures moviefeatures)]
    (matrix-l2 (matrixo/- likes likes-comp))))

(defn step
  "take a learning step.

   Get rows and mutated rows, compute the likes, compare with the
   actual value, and only keep the mutated rows if they are an
   improvement."

  []

  (let [mi            (rand-int (matrix/row-count moviefeatures))
        pi            (rand-int (matrix/row-count peoplefeatures))
        mr-g          (get-mutated-row delta moviefeatures mi)
        pr-g          (get-mutated-row delta peoplefeatures pi)
        mr            (matrix/slice moviefeatures mi)
        pr            (matrix/slice peoplefeatures pi)
        comp-like     (matrix/dot mr pr)
        comp-like-g   (matrix/dot mr-g pr-g)
        actual-like   (matrix/mget likes pi mi)]
    (when (< (Math/abs (- actual-like comp-like-g))
             (Math/abs (- actual-like comp-like)))
      (apply-mutated-row moviefeatures mi mr-g)
      (apply-mutated-row peoplefeatures pi pr-g))))

(defn learn

  [steps]

  (loop [ct  steps]
    (when-not (= ct 0)
      (step)
      (timbre/info :state (str (- steps ct) "  : " (fit)))
      (recur (dec ct)))))

;; # Running the learning
;;
;; Note: learning happens, but very slowly.
;;
;; Todo:
;; 1. try some annealing to initially take bigger steps in mutation.
;; 2. try to find some patterns in the learned features.
;; 3. compare in speed with more directly algorithms.

(defn -main [& args]
  (learn 10000))
